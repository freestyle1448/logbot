package online.cryptopie.repositories;

import online.cryptopie.models.Source;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SourceRepository extends MongoRepository<Source, ObjectId> {
    Source findBySource(String source);
}
