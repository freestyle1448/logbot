package online.cryptopie.repositories;

import online.cryptopie.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsersRepository extends MongoRepository<User, ObjectId> {
    User findByUserId(Long userId);
}
