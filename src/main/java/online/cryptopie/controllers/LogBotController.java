package online.cryptopie.controllers;

import com.google.gson.Gson;
import online.cryptopie.models.Log;
import online.cryptopie.models.LogHttp;
import online.cryptopie.services.LogBotService;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Async
public class LogBotController {
    private final Gson gson = new Gson();
    private final LogBotService logBotService;

    public LogBotController(LogBotService logBotService) {
        this.logBotService = logBotService;
    }

    @Async
    @PostMapping("/send_log")
    public void sendLog(HttpEntity<String> entity) {
        logBotService.handleLog(gson.fromJson(gson.fromJson(entity.getBody(), LogHttp.class).getMessage(), Log.class));
    }

    @Async
    @GetMapping("/")
    public void test() {
        logBotService.handleLog(Log.builder()
                .log("всё ок")
                .src("h")
                .build());
    }

    @GetMapping("favicon.ico")
    @ResponseBody
    void returnNoFavicon() {
    }
}
