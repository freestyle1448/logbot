package online.cryptopie.app;


import online.cryptopie.bots.LogBot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;

@EnableWebMvc
@SpringBootApplication
@EnableAsync
@ComponentScan(basePackages = "online.cryptopie")
@EnableMongoRepositories(basePackages = "online.cryptopie.repositories")
@EnableTransactionManagement
public class Application {
    public static void main(String[] args) throws IOException {
        ApiContextInitializer.init(); // Инициализируем апи

        SpringApplication.run(Application.class, args);

        TelegramBotsApi botapi = new TelegramBotsApi();
        try {
            botapi.registerBot(LogBot.getInstance());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
        return new MongoTransactionManager(dbFactory);
    }
}
