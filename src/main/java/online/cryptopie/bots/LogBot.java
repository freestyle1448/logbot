package online.cryptopie.bots;

import online.cryptopie.app.BotConfig;
import online.cryptopie.models.Log;
import online.cryptopie.models.Source;
import online.cryptopie.models.User;
import online.cryptopie.repositories.SourceRepository;
import online.cryptopie.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.logging.BotLogger;

import javax.annotation.PostConstruct;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static online.cryptopie.Commands.START;
import static online.cryptopie.Commands.STOP;

@Component
public class LogBot extends TelegramLongPollingBot {
    private static final List<User> users = new ArrayList<>();
    private static final String LOGTAG = "LOGBOT";
    private static final String HELP_TEXT = "Send correct command";
    private static final String ERROR_MESSAGE_TEXT = "There was an error sending the message to channel *%s*, the error was: ```%s```";
    private static LogBot INSTANCE;
    private static UsersRepository usersRepository;
    private static SourceRepository sourceRepository;

    private LogBot() {
    }

    public static LogBot getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LogBot();
        }

        return INSTANCE;
    }

    @Autowired
    private void setUsersRepository(UsersRepository usersRepository) {
        LogBot.usersRepository = usersRepository;
    }

    @Autowired
    public void setSourceRepository(SourceRepository sourceRepository) {
        LogBot.sourceRepository = sourceRepository;
    }

    @PostConstruct
    public void init() {
        final List<User> allUsers = usersRepository.findAll();
        allUsers.parallelStream().forEach(users::add);
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            Message message = update.getMessage();
            if (message != null && message.hasText()) {
                try {
                    handleIncomingMessage(message);
                } catch (InvalidObjectException e) {
                    BotLogger.severe(LOGTAG, e);
                }
            }
        } catch (Exception e) {
            BotLogger.error(LOGTAG, e);
        }
    }

    @Override
    public String getBotUsername() {
        return BotConfig.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.BOT_TOKKEN;
    }

    private void handleIncomingMessage(Message message) throws InvalidObjectException {
        User userToSave = User.builder()
                .userId(Long.valueOf(message.getFrom().getId()))
                .username(message.getFrom().getUserName())
                .build();

        switch (message.getText()) {
            case START:
                final User one = usersRepository.findByUserId(userToSave.getUserId());
                if (one == null)
                    usersRepository.save(userToSave);

                if (users.parallelStream().noneMatch(user -> user.equals(userToSave)))
                    users.add(userToSave);
                initKeyBoard(message.getChatId(), "Welcome, choose logsource");

                break;
            case STOP:
                users.remove(userToSave);
                break;
            default:
                if (message.getText().startsWith("/")) {
                    String source = message.getText().substring(1);
                    int userCollectionIndex = users.indexOf(userToSave);
                    User user = users.get(userCollectionIndex);
                    user.addSource(source);
                    users.set(userCollectionIndex, user);
                } else
                    sendHelpMessage(message.getChatId(), message.getMessageId(), null);
        }
    }

    public void sendMessageToAll(Log log) {
        users.parallelStream().forEach(user -> {
            if (user.getSources().parallelStream().anyMatch(s -> s.equals(log.getSrc()) || s.equals("ALL")))
                sendMessageToChannel(user.getUserId(), String.format("%s : %s : %tc", log.getSrc(), log.getLog(), new Date()));
        });
    }

    private void sendErrorMessage(Message message, String errorText) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        sendMessage.setText(String.format(ERROR_MESSAGE_TEXT, message.getText().trim(), errorText.replace("\"", "\\\"")));
        sendMessage.enableMarkdown(true);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }
    }


    private void initKeyBoard(Long chatId, String message) {
        final List<Source> sources = sourceRepository.findAll();
        final List<String> sourcesButton = new ArrayList<>();
        sources.add(Source.builder().source("ALL").build());
        sources.parallelStream().map(source -> "/" + source.getSource()).forEach(sourcesButton::add);


        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboardButtons = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.addAll(sourcesButton);
        keyboardButtons.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboardButtons);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        sendMessage.setText(message);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }
    }

    private void sendMessageToChannel(Long chatId, String messageText) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);

        sendMessage.setText(messageText);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }
    }

    private void sendHelpMessage(Long chatId, Integer messageId, ReplyKeyboardMarkup replyKeyboardMarkup) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setReplyToMessageId(messageId);
        if (replyKeyboardMarkup != null) {
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
        }

        sendMessage.setText(HELP_TEXT);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }
    }
}
