package online.cryptopie.services;

import online.cryptopie.bots.LogBot;
import online.cryptopie.models.Log;
import online.cryptopie.models.Source;
import online.cryptopie.repositories.LogRepository;
import online.cryptopie.repositories.SourceRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
public class LogBotServiceImpl implements LogBotService {
    private final LogRepository logRepository;
    private final SourceRepository sourceRepository;

    public LogBotServiceImpl(LogRepository logRepository, SourceRepository sourceRepository) {
        this.logRepository = logRepository;
        this.sourceRepository = sourceRepository;
    }

    @Override
    public void handleLog(Log log) {
        if (sourceRepository.findBySource(log.getSrc()) == null)
            sourceRepository.save(Source.builder()
                    .source(log.getSrc())
                    .build());

        logRepository.save(log);

        LogBot.getInstance().sendMessageToAll(log);
    }
}
