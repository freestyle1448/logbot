package online.cryptopie.services;

import online.cryptopie.models.Log;


public interface LogBotService {
    void handleLog(Log log);
}
