package online.cryptopie.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "users")
public class User {
    @Id
    private ObjectId id;
    private Long userId;
    private String username;
    @Transient
    private final List<String> sources = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId.equals(user.userId) &&
                username.equals(user.username);
    }

    public void addSource(String source) {
        if (sources.parallelStream().noneMatch(s -> s.equals(source)))
            sources.add(source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username);
    }
}
